/**
 * Description: VERY SIMPLE PROJECT
 * Version: 1.0.0
 * Author: Fornace
 * For more complex projects, split  your js functions to different files
 */

var app = {
	version: "1.0.0"
};
$(document).ready(function() {
	"use strict";
	app.init();
});

/*********************************
 * INIT
 *********************************/

app.init = function() {

	// run init code here and call 
	app.setupSomething();


	// bindings
	$("body").on("click", "title", app.testFunction);
	// DO NOT write code on the binding directly, keep them nice in one line :)

}


/*********************************
 * Example Init
 *********************************/

// execute init of libraries etc in functions like this one
app.setupSomething = function() {
	console.log("setup done");
}


/*********************************
 * Example Bound Function
 *********************************/

app.testFunction = function() {
	// "this" will be the element the user has clicked on the binding function.
	// it's automatically passed.
	console.log("You clicked on: " + this.tagName);
}