var jsFiles = [];
var cssFiles = [];

/*************************
 * JS FILES
 **************************/

jsFiles.push("src/vendor/jquery/jquery-2.1.0.min.js");
jsFiles.push("src/vendor/parallax.min.js");
jsFiles.push("src/vendor/viewportchecker/viewportchecker.js");
jsFiles.push("src/vendor/swiper/swiper.min.js");
jsFiles.push("src/js/app.js");


/*************************
 * CSS FILES
 **************************/

cssFiles.push("src/vendor/animate/animate.css");
cssFiles.push("src/vendor/normalize/normalize.css");
cssFiles.push("src/css/fonts.less");
cssFiles.push("src/css/app.less");


/*************************
 * GULP SCRIPT
 **************************/

var gulp = require('gulp');
var concat = require('gulp-concat');
var os = require('os');
var argv = require('yargs').argv;
var gulpif = require('gulp-if');
var uglify = require('gulp-uglify');
var notify = require('gulp-notify');
var gutil = require('gulp-util');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');
var connect = require('gulp-connect');
var less = require('gulp-less');
var open = require('gulp-open');
var twig = require('gulp-twig');
var sourcemaps = require('gulp-sourcemaps');


gulp.task('js', function() {
  return gulp.src(jsFiles, {
      base: 'src'
    })
    .pipe(gulpif(argv.dev, sourcemaps.init()))
    .pipe(concat('app.min.js'))
    .pipe(uglify({
      preserveComments: "none"
    }))
    .on('error', swallowError)
    .pipe(gulpif(argv.dev, sourcemaps.write()))
    .pipe(gulp.dest('build'))
    .pipe(connect.reload())
    .pipe(notify({
      message: 'Finished minifying JavaScript'
    }));
});

gulp.task('css', function() {
  return gulp.src(cssFiles, {
      base: '../src'
    })
    .pipe(gulpif(argv.dev, sourcemaps.init()))
    .pipe(less())
    .pipe(concat('app.min.css'))
    .pipe(cleanCSS({
      compatibility: 'ie9',
      level: 2,
      rebaseTo: './',
      specialComments: 0
    }))
    .on('error', swallowError)
    .pipe(autoprefixer({
      browsers: ['last 12 versions'],
      cascade: false
    }))
    .pipe(gulpif(argv.dev, sourcemaps.write()))
    .pipe(gulp.dest('build'))
    .pipe(notify({
      message: 'Finished minifying CSS'
    }));
});

gulp.task('watch', function() {
  gulp.watch('src/js/**/*.js', ['js', 'reload']);
  gulp.watch('src/css/**/*.less', ['css', 'reload']);
  gulp.watch('*.css', ['css', 'reload']);
  gulp.watch(['src/**/*.html', '*.html'], ['html', 'reload']);
});

gulp.task('html', function() {
  return gulp.src(['src/**/*.html', '!src/includes/**/*.html'])
    .pipe(twig({
      onError: function() {}
    }))
    .pipe(gulp.dest('.'))
});

gulp.task('reload', function() {
  connect.reload()
});


var browser = os.platform() === 'linux' ? 'google-chrome' : (
  os.platform() === 'darwin' ? 'google chrome' : (
    os.platform() === 'win32' ? 'chrome' : 'firefox'));

var localServer = {
  port: 8888,
  hostname: "localhost"
}

gulp.task('connect', function() {
  connect.server({
    livereload: true,
    port: localServer.port
  })

  gulp.src('index.html')
    .pipe(open({
      uri: 'http://' + localServer.hostname + ':' + localServer.port,
      app: browser
    }));

});

gulp.task('templates', function() {
  return gulp.src(['src/**/*.html', '!src/includes/**/*.html']) // run the Twig template parser on all .html files in the "src" directory
    .pipe(twig())
    .pipe(gulp.dest('.')); // output the rendered HTML files to the "dist" directory
});


function swallowError(error) {



  this.emit('end')
}


gulp.task('default', ['js', 'css', 'watch', 'html', 'templates', 'connect']);