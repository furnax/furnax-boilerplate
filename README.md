# Fornace HTML5 Boilerplate

Keep in mind it's a work in progress.

Quickly setup the environment to create nice and quick serving html5 projects.

## Features

- write common html parts like header and footer just once, then include them in the html files
- async css and javascript files
- js and css concatenation and minification
- autoprefixer
- launch and update automatically a localhost preview
- includes a nice htaccess to enable browser caching and file compression
- with the **--dev** option, it will include a minmap for the js and css
- uses Twig to allow efficient and fast frontend development
- includes animate.css and normalize.css, plus some other handy js. If you don't need them, just delete them!

## Installation and execution

[Download](https://gitlab.com/furnax/furnax-boilerplate/repository/archive.zip?ref=master) the repo and run
`npm install`

### Production

After the installation is complete, you can just run the default gulp task.
In terminal, go the main folder and type:


``` gulp ```


### Development
If you are developing, it's very useful to include **`minmaps`**, which will show you the unminified code and file source instead of the minified and concatenated file.

`gulp --dev `


## Usage

The basics:

- You work under the `src` folder;
- Write all css without prefixes;
- You must manually add new css and js files to `gulpfile.js`;
- Put template parts in `src/includes`.

All your files will be automatically compiled and **copied to the main folder**, at every change. Your css and js files will all be compiled to a single file.

Check `src/index.html` for a basic example, it's easier than you might think!

#### Adding a js or css file
If you want to add a js or css file, you have to stop gulp with `control-c` and add it at the beginning of gulpfile.js, where you see the file list. Then run `gulp` again.

##### Important Note:
Footers, headers and any other templating stuff **must** stay in the `src/includes` folder, so they are excluded from the watch tasks.



## Basic twig usage



##### Create a twig variable

```
{% set title = "Html5 Boilerplate" %}
<title>{{ title }}</title>
```

Your html file will be rendered like this

```
<title>Html5 Boilerplate</title>
```
##### Use content replacement to create a reusable document template

Create a base structure (like src/includes/default.html).
Then you can add to it both variables and content placeholders.

In src/includes/default.html

```
<head>
	<title>{{ title }}</title>
	<meta name="description" content="{{ description }}">
</head>
{% block mainContent %}
  <p>this is rendered in the main page content</p>
{% endblock %}
```

In src/index.html

```
{% extends "./includes/default.html" %}
{% set title = "This will be the title" %}
{% set description = "The will be the description" %}

{% block mainContent %}
  <p>This is rendered in the main page content</p>
{% endblock %}
```
The resulting file will have the structure of default.html but use the contents provided by index.html.

##### Include repeated html parts with twig

Create a new file, for example `sidebar.html`, and place it in `src/includes`. Now you can simply include it in several html files with an include statement:

```
{% include "./includes/sidebar.html" %}
```

Learn more about [Twig.js](https://twig.sensiolabs.org/doc/2.x/recipes.html) on the official website.


## Adding a gulp module
Add them with --save-dev option, so your coworkers can avoid errors.

## Contributing
Create a branch, then request a merge.